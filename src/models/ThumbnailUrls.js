export default class ThumbnailUrls {
  constructor({
    large = '',
    medium = '',
    square = '',
  } = {}) {
    this.large = large;
    this.medium = medium;
    this.square = square;
  }
}
