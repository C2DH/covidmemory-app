import Media from '@/models/Media';
import MemoryPoint from '@/models/MemoryPoint';
import { getValue, COLORS } from '@/models/utils';

export default class Memory extends MemoryPoint {
  constructor({
    id = 0,
    title = '',
    content = '',
    creator = '',
    email = '',
    pattern = [],
    date = new Date(),
    dateCreated = new Date(),
    dateModified = new Date(),
    media = [],
    place = '',
    basedNear = {},
  } = {}) {
    super({
      id,
      title,
      creator,
      date,
      dateCreated,
      dateModified,
      basedNear,
    });
    this.email = String(email);
    this.content = String(content).trim();
    this.place = String(place).trim();

    this.pattern = pattern;
    this.scale = 1.5 * Math.random() + 1;
    this.color = COLORS[(this.id % (COLORS.length - 1))];
    this.media = media;
  }

  static create(item) {
    console.info('Memory.create', item);
    return new Memory({
      id: item['o:id'],
      title: item['o:title'],
      content: getValue(item, 'dcterms:description'),
      dateCreated: new Date(getValue(item, 'o:created')),
      dateModified: new Date(getValue(item, 'o:modified')),
      date: new Date(getValue(item, 'dcterms:created')),
      creator: getValue(item, 'dcterms:creator'),
      place: getValue(item, 'dcterms:spatial'),
      basedNear: getValue(item, 'foaf:based_near'),
      media: item['o:media'].map((m) => Media.create(m)),
    });
  }

  getExcerpt({ maxLength = 150, text = '' } = {}) {
    const excerpt = (text.length ? text : this.content)
      .replace(/(\r\n|\n|\r)+/gm, ' ')
      .replace(/(<([^>]+)>)/gi, '');
    if (excerpt.length < maxLength) {
      return excerpt;
    }
    let trimmed = excerpt.slice(0, maxLength);
    trimmed = trimmed.slice(0, Math.min(trimmed.length, trimmed.lastIndexOf(' ')));
    return `${trimmed}…`;
  }

  getFirstMedia() {
    if (!this.media.length) {
      return new Media();
    }
    return this.media[this.media.length - 1];
  }

  getThumbnail(size = 'square') {
    if (!this.media.length) {
      return '';
    }
    return this.media[this.media.length - 1].thumbnailUrls[size];
  }

  setMedia(media = []) {
    this.media = media;
  }
}
