export default {
  namespaced: true,
  state: {
    language: 'en',
    cookiesAccepted: false,
  },
  mutations: {
    SET_LANGUAGE(state, value) {
      state.language = value;
      console.info('SET_LANGUAGE mutate', value);
    },
    SET_COOKIES_ACCEPTED(state) {
      state.cookiesAccepted = true;
      console.info('SET_COOKIES_ACCEPTED cookiesAccepted');
    },
  },
  actions: {
    ACCEPT_COOKIES({ commit }) {
      commit('SET_COOKIES_ACCEPTED');
    },
    SET_LANGUAGE({ commit }, value) {
      console.info('SET_LANGUAGE', value);
      commit('SET_LANGUAGE', value);
    },
  },
};
